const express = require('express')
const cobaRoutes = require('./main/routes/coba.routes')
const movieRoutes = require('./main/routes/movie.routes')
const userRoutes = require('./main/routes/user.routes')
const swaggerJsDoc = require('swagger-jsdoc')
const swaggerUi = require('swagger-ui-express')
const app = express()
require('./main/middleware/middleware')(app)

const swaggerOptions = {
  swaggerDefinition : {
    info: {
      title: "Lich Movie DataBase API",
      description: "User and Movie API",
      contact: {
        name: "priyoiyo",
        phone: "rahasia"
      },
      servers: ["localhost:3002"]
    }
  },
  apis: ["index.js"]
}

const swaggerDocs = swaggerJsDoc(swaggerOptions);

app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocs))

// Routes
/**
 * @swagger
 * /users:
 *  get:
 *    description: Use to request all users
 *    responses:
 *      '200':
 *        description: A successful response
 * 

 * 
 * 
 * 
 */  
 /** /users/{id}:
 *  get:
 *    description: Use to request all users
 *    responses:
 *      '200':
 *        description: A successful response
 **/



app.get('/users', userRoutes.getUserAPI)
app.get('/users/:id', userRoutes.getUser)
app.post('/users', userRoutes.createUser)
app.put('/users/:id', userRoutes.updateUser)
app.delete('/users/:id', userRoutes.deleteUser)
app.post('/users/login', userRoutes.loginUser)


app.get('/movies', movieRoutes.getMovieAPI)
app.get('/movies/:id', movieRoutes.getMovie)
app.post('/movies', movieRoutes.createMovie)
app.put('/movies/:id', movieRoutes.updateMovie)
app.delete('/movies/:id', movieRoutes.deleteMovie)


app.get('/coba', cobaRoutes.cobaAPI)


// Start server
app.listen(process.env.PORT || 3002, () => {
  console.log(`Server listening on Port`)
})