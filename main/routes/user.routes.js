const { pool } = require('../../config')
var bcrypt = require('bcryptjs')
var jwt = require('jsonwebtoken')
var jwtPass = 'rahasia'


const getUserAPI = (request, response) => {
  pool.query('SELECT * FROM users', (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}

const getUser = (request, response) => {
  const id = parseInt(request.params.id)
  pool.query('SELECT * FROM users WHERE id = $1', [id], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows[0])
  })

}

function queryUpdateUser  (cols) {
  // const id = parseInt(request.params.id)
  let query = ['UPDATE users'];
  query.push('SET');
  let set = []
  let total = []
  Object.keys(cols).forEach(function (key, i) {
    set.push(key + ' = $' + (i + 1)); 
  });
  total.push(set);
  query.push(set.join(', '));
  let id = total[0].length + 1
  query.push('WHERE id = $' + id);
  return query.join(' ');

}

const updateUser = (request, response) => {
  const id = parseInt(request.params.id)
  var query = queryUpdateUser(request.body);
  var colValues = Object.keys(request.body).map(function (key) {
    return request.body[key];
  });
  colValues.push(id)

  pool.query(`${query}`, colValues,
  (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).send(`user modified with ID: ${id}`)
  })
}


const createUser = async (request, response) => {
const {username, password_user, email_user} = request.body
const hashPassword = bcrypt.hashSync(password_user, 10)
// 'admin' request.body.username
// console.log(pool.query('SELECT username FROM users WHERE username = $1', ['adminoce']))
const { rows } = await pool.query('SELECT username FROM users WHERE username = $1', [username])
const validasi = await validateUser(username, password_user)

if (validasi){
    let cekUser = rows[0]
    if (typeof cekUser == 'undefined'){
      pool.query('INSERT INTO users (username, password_user, email_user) VALUES ($1, $2, $3)', [username, hashPassword, email_user], error => {
        if (error) {
          throw error
        }
        response.status(201).json({ status: 'success', message: 'User added.' })
      })
    } else if (typeof cekUser == 'object') {
      response.status(409).send(`username ${cekUser.username} already exist`)
    } else{
      response.status(502).send(`Error`)
    }
} else {
  response.status(502).send(`Inputan Salah`)
}
  
}


const deleteUser = (request, response) => {
  const id = parseInt(request.params.id)
  pool.query('DELETE FROM users WHERE id = $1', [id], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).send(`user deleted with ID: ${id}`)
  })
}

const loginUser = async (request, response) => {
  const {username, password_user} = request.body

  const { rows } = await pool.query('SELECT username, password_user, role_user, id FROM users WHERE username = $1', [username])
  let cekUser = rows[0]
        if (typeof cekUser == 'undefined'){
          response.status(409).send(`username tidak ada`)
        } else if (typeof cekUser == 'object') {
          // response.status(502).send(`login`)
          var hash = bcrypt.compareSync(password_user, cekUser.password_user)
          // response.status(409).send(`${cekUser.password_user}`)
                    if (hash) {
                      var token = jwt.sign({
                          username: cekUser.username,
                          id: cekUser.id
                          //_id merujuk ke id yg di generate otomatis oleh mongoDB
                      }, jwtPass, { expiresIn: '1h' })
                      //jwtPass adalah password random untuk crypt token jwtnya
                      response.status(200).json({
                        success: 'success',
                        message: 'berhasil login',
                        token: token,
                        userId: cekUser.id,
                        role_user: cekUser.role_user
                      })

                  } else {
                    response.status(502).send(`Password Salah`)
                  }
        } else{
          response.status(502).send(`Error`)
        }
}

function validateUser(cekusername, cekpassword_user){
  const validUserName = typeof cekusername == 'string' && !cekusername.includes(" ");
  const validPassword = typeof cekpassword_user == 'string' && !cekpassword_user.includes(" ") && cekpassword_user.trim().length >= 6;
  return validUserName && validPassword
}

module.exports = {
  getUserAPI,
  getUser,
  createUser,
  updateUser,
  deleteUser,
  loginUser
}