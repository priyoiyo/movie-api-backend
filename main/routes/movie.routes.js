const { pool } = require('../../config')

const getMovieAPI = (request, response) => {
  pool.query('SELECT * FROM movies', (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}

const getMovie = (request, response) => {
  const id = parseInt(request.params.id)
  pool.query('SELECT * FROM movies WHERE id = $1', [id], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows[0])
  })

}

function queryUpdateMovie  (cols) {
  // const id = parseInt(request.params.id)
  let query = ['UPDATE movies'];
  query.push('SET');
  let set = []
  let total = []
  Object.keys(cols).forEach(function (key, i) {
    set.push(key + ' = $' + (i + 1)); 
  });
  total.push(set);
  query.push(set.join(', '));
  let id = total[0].length + 1
  query.push('WHERE id = $' + id);
  return query.join(' ');

}

const updateMovie = (request, response) => {
  const id = parseInt(request.params.id)
  var query = queryUpdateMovie(request.body);
  var colValues = Object.keys(request.body).map(function (key) {
    return request.body[key];
  });
  colValues.push(id)

  pool.query(`${query}`, colValues,
  (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).send(`Movie modified with ID: ${id}`)
  })
}



const deleteMovie = (request, response) => {
  const id = parseInt(request.params.id)
  pool.query('DELETE FROM movies WHERE id = $1', [id], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).send(`Movie deleted with ID: ${id}`)
  })
}


const createMovie = (request, response) => {
  const { title, genre, country, producer, status_movie, cast_movie, detail_movie, img_movie } = request.body

  pool.query('INSERT INTO movies (title, genre, country, producer, status_movie, cast_movie, detail_movie, img_movie) VALUES ($1, $2, $3, $4, $5, $6, $7, $8)', [title, genre, country, producer, status_movie, cast_movie, detail_movie, img_movie], error => {
    if (error) {
      throw error
    }
    response.status(201).json({ status: 'success', message: 'Movie added.' })
  })
}


module.exports = {
  getMovieAPI,
  getMovie,
  createMovie,
  updateMovie,
  deleteMovie,
}