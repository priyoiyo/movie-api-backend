const { pool } = require('../../config')

const getFavoriteAPI = (request, response) => {
  pool.query('SELECT * FROM favorites', (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}

const getFavorite = (request, response) => {
  const id = parseInt(request.params.id)
  pool.query('SELECT * FROM favorites WHERE id = $1', [id], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows[0])
  })

}

function queryUpdateFavorite  (cols) {
  // const id = parseInt(request.params.id)
  let query = ['UPDATE favorites'];
  query.push('SET');
  let set = []
  let total = []
  Object.keys(cols).forEach(function (key, i) {
    set.push(key + ' = $' + (i + 1)); 
  });
  total.push(set);
  query.push(set.join(', '));
  let id = total[0].length + 1
  query.push('WHERE id = $' + id);
  return query.join(' ');

}

const updateFavorite = (request, response) => {
  const id = parseInt(request.params.id)
  var query = queryUpdateFavorite(request.body);
  var colValues = Object.keys(request.body).map(function (key) {
    return request.body[key];
  });
  colValues.push(id)

  pool.query(`${query}`, colValues,
  (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).send(`favorite modified with ID: ${id}`)
  })
}


const deleteFavorite = (request, response) => {
  const id = parseInt(request.params.id)
  pool.query('DELETE FROM favorites WHERE id = $1', [id], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).send(`favorite deleted with ID: ${id}`)
  })
}


const createFavorite = (request, response) => {
  const { favoritename, password_favorite, email_favorite} = request.body

  pool.query('INSERT INTO favorites (favoritename, password_favorite, email_favorite) VALUES ($1, $2, $3)', [favoritename, password_favorite, email_favorite], error => {
    if (error) {
      throw error
    }
    response.status(201).json({ status: 'success', message: 'favorite added.' })
  })
}


module.exports = {
  getFavoriteAPI,
  getFavorite,
  createFavorite,
  updateFavorite,
  deleteFavorite,
}