var jwt = require('jsonwebtoken')
var jwtPass = 'rahasia'
const { pool } = require('../../config')


module.exports = (request, response, next) => {
    if (request.headers.token) {
        //header.token adalah headers di ppostman, buat key token, dan valuenyatoken yg didapat dr login
        jwt.verify(request.headers.token, jwtPass, (err, decoded) => {
            //setelah token ditangkap, di decode dgn jwt veirfy
            if (err) {
                response.status(500).json({
                    success: 'Failed',
                    message: 'error'
                  })
            } else {
                User.findById(decoded.id)
                    //decoded.id cek di controller login. ketika token di decode, dia kembali menjadi data  yg kita code di login
                    .then(user => {
                        if (user) {
                            request.userId = user._id
                            request.username = user.username
                            //kita menyimpan user._id (data yg di tokenin), menjadi request.userId agar bisa dipanggil di controller
                            next()
                            //middleware wajib ada next, biar klo logic midle ware selesai, dia melanjutkan ke routes nya
                        } else {
                            response.status(502).json({
                                success: 'failed',
                                message: 'user not found'
                              })
                        }
                    })
                    .catch(decodeErr => {
                        response.status(502).json({
                            success: 'failed',
                            message: decodeErr
                          })
                    })
            }
        })
    } else {
        response.status(502).json({
            success: 'failed',
            message: 'masukkan Token'
          })
    }
}