require('dotenv').config()
const compression = require('compression')
const helmet = require('helmet')
const bodyParser = require('body-parser')
const rateLimit = require('express-rate-limit')
const cors = require('cors')
// const bcrypt = require('bcrypt');
// const saltRounds = 10;
// module.exports.bcrypt = bcrypt;

const limiter = rateLimit({
    windowMs: 1 * 60 * 1000, // 1 minute
    max: 50, // 50 requests,
  })

const isProduction = process.env.NODE_ENV === 'production'
const origin = {
    origin: isProduction ? 'https://www.example.com' : '*',
  }

module.exports = (app) => {
    app.use(compression())
    app.use(helmet())
    app.use(bodyParser.json())
    app.use(bodyParser.urlencoded({ extended: true }))
    app.use(limiter)
    app.use(cors(origin))
    
}