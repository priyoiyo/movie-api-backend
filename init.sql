CREATE TABLE IF NOT EXISTS movies (
  id SERIAL PRIMARY KEY,
  title VARCHAR(255) NOT NULL,
  genre VARCHAR[],
  country VARCHAR(255),
  producer VARCHAR[],   
  status_movie VARCHAR(255),
  cast_movie VARCHAR[],
  detail_movie VARCHAR,
  img_movie VARCHAR(255) DEFAULT 'https://www.capitalkaratesc.com/wp-content/uploads/2017/04/default-image.jpg',
  likes INTEGER DEFAULT 0,
  created_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS users (
  id SERIAL PRIMARY KEY,
  username VARCHAR(50) NOT NULL,
  password_user VARCHAR NOT NULL,
  email_user VARCHAR(50) NOT NULL,
  role_user VARCHAR(10) DEFAULT 'user',
  img_user VARCHAR(255) DEFAULT 'https://www.capitalkaratesc.com/wp-content/uploads/2017/04/default-image.jpg'
  
);

CREATE TABLE IF NOT EXISTS favorites (
  id BIGSERIAL PRIMARY KEY,
  likes INTEGER DEFAULT 0,
  favorite BOOLEAN DEFAULT FALSE,
  user_id INTEGER NOT NULL,
  movie_id INTEGER NOT NULL,
  FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE,
  FOREIGN KEY (movie_id) REFERENCES movies(id) ON DELETE CASCADE

);

INSERT INTO users (username, password_user, email_user, role_user, img_user)
VALUES  ('adminoce', '123456', 'admin@admin.com', 'adminoce', 'https://www.capitalkaratesc.com/wp-content/uploads/2017/04/default-image.jpg'
);

INSERT INTO movies (title, genre, country, producer, status_movie, cast_movie, detail_movie, img_movie)
VALUES  ('Harry Potter', '{"horor", "mystery"}', 'United Kingdom', '{"J.K. Rowling"}', 'Published', '{"Doraemon: asda", "Sadako As Auditor"}', 'Lorem Ipsum Gona Parte Es Silas Valentinus Albertus Yodosakuso Lorem Ipsum Gona Parte Es Silas Valentinus Albertus Yodosakuso', 'https://www.capitalkaratesc.com/wp-content/uploads/2017/04/default-image.jpg');