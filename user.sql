CREATE TABLE IF NOT EXISTS users (
  id SERIAL PRIMARY KEY,
  username VARCHAR(50) NOT NULL,
  password_user VARCHAR(50) NOT NULL,
  email_user VARCHAR(50) NOT NULL,
  role_user VARCHAR(10) DEFAULT 'user',
  img_user VARCHAR(255) DEFAULT 'https://www.capitalkaratesc.com/wp-content/uploads/2017/04/default-image.jpg'
);

INSERT INTO users (username, password_user, email_user, role_user, img_user)
VALUES  ('adminoce', '123456', 'admin@admin.com', 'adminoce', 'https://www.capitalkaratesc.com/wp-content/uploads/2017/04/default-image.jpg'
);