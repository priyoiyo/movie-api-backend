const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const { pool } = require('./config')
const helmet = require('helmet')
const compression = require('compression')
const rateLimit = require('express-rate-limit')
const { body, check, validationResult } = require('express-validator')

const app = express()

app.use(compression())
app.use(helmet())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(cors())
const isProduction = process.env.NODE_ENV === 'production'
const origin = {
  origin: isProduction ? 'https://www.example.com' : '*',
}

app.use(cors(origin))

const limiter = rateLimit({
    windowMs: 1 * 60 * 1000, // 1 minute
    max: 5, // 5 requests,
  })
const postLimiter = rateLimit({
    windowMs: 1 * 60 * 1000,
    max: 5,
  })

  app.use(limiter)
  

const getMovieAPI = (request, response) => {
  pool.query('SELECT * FROM movies', (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}

const getMovie = (request, response) => {
    const id = parseInt(request.params.id)
    pool.query('SELECT * FROM movies WHERE id = $1', [id], (error, results) => {
      if (error) {
        throw error
      }
      response.status(200).json(results.rows)
    })
  }

const updateMovie = (request, response) => {
    const id = parseInt(request.params.id)
    const { title, genre, country, producer, status_movie, cast_movie, detail_movie, img_movie, likes} = request.body

    pool.query('UPDATE movies SET title = $1, genre = $2, country = $3, producer = $4, status_movie = $5, cast_movie =$6, detail_movie = $7, img_movie = $8, likes = $9 WHERE id = $10', 
    [title, genre, country, producer, status_movie, cast_movie, detail_movie, img_movie, likes, id],
    (error, results) => {
      if (error) {
        throw error
      }
      response.status(200).send(`Movie modified with ID: ${id}`)
    })
  }

const deleteMovie = (request, response) => {
    const id = parseInt(request.params.id)
    pool.query('DELETE FROM movies WHERE id = $1', [id], (error, results) => {
        if (error) {
        throw error
        }
        response.status(200).send(`Movie deleted with ID: ${id}`)
        })
    }


app
  .route('/movies')
  // GET endpoint
  .get(getMovieAPI)

app.get('/movies/:id', getMovie)
app.put('/movies/:id', updateMovie)
app.delete('/movies/:id', deleteMovie)
  // POST endpoint
app.post('/movies',
 [
    check('title')
      .not()
      .isEmpty()
      .isLength({ min: 3, max: 255 })
      .trim(),
  ],
    postLimiter,
    (request, response) => {
        const errors = validationResult(request)
    
        if (!errors.isEmpty()) {
          return response.status(422).json({ errors: errors.array() })
        }
    
        const { title, genre, country, producer, status_movie, cast_movie, detail_movie, img_movie} = request.body
    
        pool.query('INSERT INTO movies (title, genre, country, producer, status_movie, cast_movie, detail_movie, img_movie) VALUES ($1, $2, $3, $4, $5, $6, $7, $8)', [title, genre, country, producer, status_movie, cast_movie, detail_movie, img_movie], error => {
          if (error) {
            throw error
          }
          response.status(201).json({ status: 'success', message: 'Movie added.' })
        })
      }
    )

// Start server
app.listen(process.env.PORT || 3002, () => {
  console.log(`Server listening on Port`)
})