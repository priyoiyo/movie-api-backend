const { pool } = require('../../config')

const getMovieAPI = (request, response) => {
    pool.query('SELECT * FROM movies', (error, results) => {
      if (error) {
        throw error
      }
      response.status(200).json(results.rows)
    })
  }
  
  const getMovie = (request, response) => {
      const id = parseInt(request.params.id)
      pool.query('SELECT * FROM movies WHERE id = $1', [id], (error, results) => {
        if (error) {
          throw error
        }
        response.status(200).json(results.rows)
      })
      
    }
  
  const updateMovie = (request, response) => {
      const id = parseInt(request.params.id)
      const { title, genre, country, producer, status_movie, cast_movie, detail_movie, img_movie, likes} = request.body

      pool.query('UPDATE movies SET title = $1, genre = $2, country = $3, producer = $4, status_movie = $5, cast_movie =$6, detail_movie = $7, img_movie = $8, likes = $9 WHERE id = $10', 
      [title, genre, country, producer, status_movie, cast_movie, detail_movie, img_movie, likes, id],
      (error, results) => {
        if (error) {
          throw error
        }
        response.status(200).send(`Movie modified with ID: ${id}`)
      })
    }
  
  const deleteMovie = (request, response) => {
      const id = parseInt(request.params.id)
      pool.query('DELETE FROM movies WHERE id = $1', [id], (error, results) => {
          if (error) {
          throw error
          }
          response.status(200).send(`Movie deleted with ID: ${id}`)
          })
      }
  

const createMovie = (request, response) => {
    const { title, genre, country, producer, status_movie, cast_movie, detail_movie, img_movie} = request.body
      
    pool.query('INSERT INTO movies (title, genre, country, producer, status_movie, cast_movie, detail_movie, img_movie) VALUES ($1, $2, $3, $4, $5, $6, $7, $8)', [title, genre, country, producer, status_movie, cast_movie, detail_movie, img_movie], error => {
      if (error) {
        throw error
      }
      response.status(201).json({ status: 'success', message: 'Movie added.' })
    })
    }


module.exports = {
    getMovieAPI,
    getMovie,
    createMovie,
    updateMovie,
    deleteMovie,
}